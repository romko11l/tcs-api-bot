\c tinkinvestbot

CREATE TABLE orderbook_history(
    id             UUID PRIMARY KEY,
    instrument_uid TEXT NOT NULL,
    date           TIMESTAMPTZ NOT NULL,
    bids_quantity  BIGINT[] NOT NULL,
    bids_units     BIGINT[] NOT NULL,
    bids_nano      INT[] NOT NULL,
    asks_quantity  BIGINT[] NOT NULL,
    asks_units     BIGINT[] NOT NULL,
    asks_nano      INT[] NOT NULL
);

GRANT ALL PRIVILEGES ON orderbook_history TO tinkinvestbot;

CREATE TABLE subscriptions(
    instrument_uid TEXT PRIMARY KEY,
    name           TEXT NOT NULL,
    is_active      BOOL NOT NULL
);

GRANT ALL PRIVILEGES ON subscriptions TO tinkinvestbot;
