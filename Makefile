.PHONY: build
build: # Build debug binaries
	cargo build

.PHONY: build-release
build-release: # Build release binaries
	cargo build --release

.PHONY: fmt
fmt: # Formate code according to style guidelines
	cargo fmt

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?# .*$$' ./Makefile | sort | awk \
		'BEGIN {FS = ":.*?# "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'
