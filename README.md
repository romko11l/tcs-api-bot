# Run db for local tests

```bash
docker run --name TCS_POSTGRES --net=host -e POSTGRES_PASSWORD=password -d postgres:14

psql -U postgres -h localhost -p 5432 < ./bootstrap/db.sql

psql -U postgres -h localhost -p 5432 < ./bootstrap/orderbook.sql
```