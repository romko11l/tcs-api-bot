use crate::db;
use crate::service;
use crate::tcs_grpc::GetOrderBookRequest;

use metrics::counter;
use tokio_util::sync::CancellationToken;

pub struct OrderBookCollector {
    service: service::TinkoffInvestService,
    connector: db::DBConnector,
}

impl OrderBookCollector {
    pub fn new(tcs_token: String, tcs_address: String, connector: db::DBConnector) -> Self {
        let service = service::TinkoffInvestService::new(tcs_token, tcs_address);
        Self {
            service: service,
            connector: connector,
        }
    }

    async fn collect_info(&self) -> Result<(), Box<dyn std::error::Error>> {
        let subscription_ids = self.connector.get_subscription_ids(true).await?;
        let channel = self.service.create_channel().await?;
        let mut market_data_service = self.service.marketdata(channel).await?;

        for id in subscription_ids.iter() {
            let request = GetOrderBookRequest {
                figi: String::from(""),
                depth: 20,
                instrument_id: id.clone(),
            };

            let ob = match market_data_service.get_order_book(request).await {
                Ok(ob) => ob.into_inner(),
                Err(err) => {
                    counter!("tcs_errors", 1);
                    return Err(Box::new(err));
                }
            };
            match self.connector.add_order_book(ob.try_into()?).await {
                Ok(()) => {}
                Err(err) => {
                    counter!("pg_errors", 1);
                    return Err(err);
                }
            };
        }

        Ok(())
    }

    pub async fn start_collecting(&self, token: CancellationToken) {
        loop {
            tokio::select! {
                _ = token.cancelled() => {
                    return
                }
                _ = tokio::time::sleep(std::time::Duration::from_secs(20)) => {
                    match self.collect_info().await{
                        Ok(()) => {
                            log::info!("Order books saved successfully");
                        },
                        Err(err) => {
                            log::error!("Can't collect order book info: {}", err);
                        },
                    }
                }
            }
        }
    }
}
