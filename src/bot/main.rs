use tinkinvestbot::db;
use tinkinvestbot::periodic_tasks::OrderBookCollector;

use chrono;
use env_logger;
use log;
use metrics::{counter, gauge, Label};
use metrics_exporter_prometheus::PrometheusBuilder;
use serde::Deserialize;
use std::{io::Write, net::SocketAddr, process};
use tokio::signal;
use tokio_util::sync::CancellationToken;

#[derive(Deserialize)]
struct Config {
    tcs_token: String,
    tcs_address: String,
    pg_address: String,
    prometheus_address: Option<String>,
}

#[tokio::main]
async fn main() {
    env_logger::Builder::new()
        .format(|buf, record| {
            writeln!(
                buf,
                "{} [{}] - {}",
                chrono::Local::now().format("%Y-%m-%dT%H:%M:%S"),
                record.level(),
                record.args()
            )
        })
        .filter(None, log::LevelFilter::Info)
        .init();

    let args: Vec<String> = std::env::args().collect();
    let conf_path = match args.get(1) {
        Some(path) => path,
        None => {
            log::error!("Pass config file");
            process::exit(1);
        }
    };

    let conf_reader = match std::fs::File::open(conf_path) {
        Ok(file) => file,
        Err(err) => {
            log::error!("Can't open config file: {}", err);
            process::exit(1);
        }
    };

    let config: Config = match serde_yaml::from_reader(conf_reader) {
        Ok(config) => config,
        Err(err) => {
            log::error!("Can't read correct config: {}", err);
            process::exit(1);
        }
    };

    match config.prometheus_address {
        Some(addr) => {
            let prometheus_addr: SocketAddr = match addr.parse() {
                Ok(addr) => addr,
                Err(err) => {
                    log::error!("Can't parse bind address: {}", err);
                    process::exit(1);
                }
            };

            match PrometheusBuilder::new()
                .with_http_listener(prometheus_addr)
                .install()
            {
                Ok(()) => {}
                Err(err) => {
                    log::error!("Can't start prometheus exporter: {}", err);
                    process::exit(1);
                }
            }

            let labels = vec![Label::new("version", env!("CARGO_PKG_VERSION"))];
            gauge!("metainfo", 1.0, labels);
            counter!("tcs_errors", 0);
            counter!("pg_errors", 0);
        }
        None => {}
    };

    let token = CancellationToken::new();

    let connector = match db::DBConnector::new(config.pg_address, token.clone()).await {
        Ok(connector) => connector,
        Err(err) => {
            log::error!("Can't connect with database: {}", err);
            process::exit(1);
        }
    };

    let collector = OrderBookCollector::new(config.tcs_token, config.tcs_address, connector);

    let cloned_token = token.clone();
    let task = tokio::spawn(async move {
        collector.start_collecting(cloned_token).await;
    });

    tokio::select! {
        _ = token.cancelled() => {},
        _ = signal::ctrl_c() => {
            token.cancel()
        },
    }

    match task.await {
        Ok(()) => {}
        Err(err) => {
            log::error!("Error occurred during shutdown: {}", err);
        }
    }

    log::info!("Tinkivestbot ends its work");
}
