use crate::model::{Instrument, Order, OrderBook, Quotation, Subscription};

use chrono::{DateTime, Local};
use tokio::sync::Mutex;
use tokio_postgres::{self, NoTls};
use tokio_util::sync::CancellationToken;
use uuid::Uuid;

#[derive(Debug)]
pub struct DBConnector {
    client: Mutex<tokio_postgres::Client>,
}

impl DBConnector {
    pub async fn new(
        address: String,
        token: CancellationToken,
    ) -> Result<Self, Box<dyn std::error::Error>> {
        let (client, connection) = tokio_postgres::connect(address.as_str(), NoTls).await?;

        tokio::spawn(async move {
            if let Err(err) = connection.await {
                log::error!("Can't connect with postgres: {}", err);
                token.cancel();
            }
        });

        let client = Mutex::new(client);

        Ok(Self { client })
    }

    pub async fn add_order_book(&self, ob: OrderBook) -> Result<(), Box<dyn std::error::Error>> {
        let query = "INSERT INTO orderbook_history
                    (id, instrument_uid, date, bids_quantity, bids_units, bids_nano, asks_quantity, asks_units, asks_nano)
                    VALUES
                    ($1, $2, $3, $4, $5, $6, $7, $8, $9);";

        let bids_quantity: Vec<i64> = ob.bids.iter().map(|order| order.quantity).collect();
        let bids_units: Vec<i64> = ob.bids.iter().map(|order| order.price.units).collect();
        let bids_nano: Vec<i32> = ob.bids.iter().map(|order| order.price.nano).collect();

        let asks_quantity: Vec<i64> = ob.asks.iter().map(|order| order.quantity).collect();
        let asks_units: Vec<i64> = ob.asks.iter().map(|order| order.price.units).collect();
        let asks_nano: Vec<i32> = ob.asks.iter().map(|order| order.price.nano).collect();

        self.client
            .lock()
            .await
            .execute(
                query,
                &[
                    &ob.id,
                    &ob.instrument_uid,
                    &ob.timemstamptz,
                    &bids_quantity,
                    &bids_units,
                    &bids_nano,
                    &asks_quantity,
                    &asks_units,
                    &asks_nano,
                ],
            )
            .await?;
        Ok(())
    }

    pub async fn get_subscription_ids(
        &self,
        active: bool,
    ) -> Result<Vec<String>, Box<dyn std::error::Error>> {
        let query = "SELECT instrument_uid FROM subscriptions WHERE is_active = $1;";

        let rows = self.client.lock().await.query(query, &[&active]).await?;
        Ok(rows.iter().map(|row| row.get(0)).collect())
    }

    pub async fn add_subscription(
        &self,
        sub: Subscription,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let query = "INSERT INTO subscriptions
                     (instrument_uid, name, is_active)
                     VALUES
                     ($1, $2, $3);";

        self.client
            .lock()
            .await
            .execute(query, &[&sub.instrument_uid, &sub.name, &sub.is_active])
            .await?;
        Ok(())
    }

    pub async fn change_subscription_status(
        &self,
        instrument_uid: String,
        status: bool,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let query = "UPDATE subscriptions
                     SET is_active = $1
                     WHERE instrument_uid = $2;";

        self.client
            .lock()
            .await
            .execute(query, &[&status, &instrument_uid])
            .await?;
        Ok(())
    }

    pub async fn get_all_subscriptions(
        &self,
    ) -> Result<Vec<Subscription>, Box<dyn std::error::Error>> {
        let query = "SELECT instrument_uid, name, is_active FROM subscriptions;";

        let rows = self.client.lock().await.query(query, &[]).await?;
        Ok(rows
            .iter()
            .map(|row| Subscription {
                instrument_uid: row.get(0),
                name: row.get(1),
                is_active: row.get(2),
            })
            .collect())
    }

    pub async fn get_order_book(&self, id: Uuid) -> Result<OrderBook, Box<dyn std::error::Error>> {
        let query = "SELECT id, instrument_uid, date, bids_quantity, bids_units, bids_nano, asks_quantity, asks_units, asks_nano
                     FROM orderbook_history
                     WHERE id = $1;";

        let row = self.client.lock().await.query_one(query, &[&id]).await?;

        let bids_quantity: Vec<i64> = row.get(3);
        let bids_units: Vec<i64> = row.get(4);
        let bids_nano: Vec<i32> = row.get(5);

        let asks_quantity: Vec<i64> = row.get(6);
        let asks_units: Vec<i64> = row.get(7);
        let asks_nano: Vec<i32> = row.get(8);

        let mut bids: Vec<Order> = Vec::new();
        let mut asks: Vec<Order> = Vec::new();
        for i in 0..bids_quantity.len() {
            bids.push(Order {
                quantity: bids_quantity[i],
                price: Quotation {
                    units: bids_units[i],
                    nano: bids_nano[i],
                },
            });

            asks.push(Order {
                quantity: asks_quantity[i],
                price: Quotation {
                    units: asks_units[i],
                    nano: asks_nano[i],
                },
            });
        }

        Ok(OrderBook {
            id: row.get(0),
            instrument_uid: row.get(1),
            timemstamptz: row.get(2),
            bids: bids,
            asks: asks,
        })
    }

    pub async fn get_available_instruments(
        &self,
        start: DateTime<Local>,
        end: DateTime<Local>,
    ) -> Result<Vec<Instrument>, Box<dyn std::error::Error>> {
        let query = "SELECT DISTINCT(orderbook_history.instrument_uid), subscriptions.name
                     FROM orderbook_history JOIN subscriptions
                     ON orderbook_history.instrument_uid = subscriptions.instrument_uid
                     WHERE orderbook_history.date >= $1
                     AND orderbook_history.date <= $2;";

        let rows = self
            .client
            .lock()
            .await
            .query(query, &[&start, &end])
            .await?;

        Ok(rows
            .iter()
            .map(|row| Instrument {
                instrument_uid: row.get(0),
                name: row.get(1),
            })
            .collect())
    }

    pub async fn get_available_order_book_ids_by_instrument_uid(
        &self,
        instrument_uid: String,
        start: DateTime<Local>,
        end: DateTime<Local>,
    ) -> Result<Vec<Uuid>, Box<dyn std::error::Error>> {
        let query = "SELECT id FROM orderbook_history
                     WHERE instrument_uid = $1
                     AND date >= $2
                     And date <= $3";

        let rows = self
            .client
            .lock()
            .await
            .query(query, &[&instrument_uid, &start, &end])
            .await?;

        Ok(rows.iter().map(|row| row.get(0)).collect())
    }
}
