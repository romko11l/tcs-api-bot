use crate::tcs_grpc::{
    instruments_service_client::InstrumentsServiceClient,
    market_data_service_client::MarketDataServiceClient,
    market_data_stream_service_client::MarketDataStreamServiceClient,
};

use std::str;

use tonic::{
    codegen::InterceptedService,
    service::Interceptor,
    transport::{Channel, ClientTlsConfig},
    Status,
};

#[derive(Debug)]
pub struct DefaultInterceptor {
    token: String,
}

impl Interceptor for DefaultInterceptor {
    fn call(&mut self, request: tonic::Request<()>) -> Result<tonic::Request<()>, Status> {
        let mut req = request;
        req.metadata_mut().append(
            "authorization",
            format!("bearer {}", self.token).parse().unwrap(),
        );

        Ok(req)
    }
}

fn string_to_static_str(s: String) -> &'static str {
    Box::leak(s.into_boxed_str())
}

#[derive(Debug)]
pub struct TinkoffInvestService {
    token: String,
    address: String,
}

impl TinkoffInvestService {
    pub fn new(token: String, address: String) -> Self {
        Self {
            token: token,
            address: address,
        }
    }

    pub async fn create_channel(&self) -> Result<Channel, Box<dyn std::error::Error>> {
        let tls = ClientTlsConfig::new();

        let channel = Channel::from_static(string_to_static_str(self.address.clone()))
            .tls_config(tls)?
            .connect()
            .await?;

        Ok(channel)
    }

    pub async fn instruments(
        &self,
        channel: Channel,
    ) -> Result<
        InstrumentsServiceClient<InterceptedService<Channel, DefaultInterceptor>>,
        Box<dyn std::error::Error>,
    > {
        let client = InstrumentsServiceClient::with_interceptor(
            channel,
            DefaultInterceptor {
                token: self.token.clone(),
            },
        );

        Ok(client)
    }

    pub async fn marketdata(
        &self,
        channel: Channel,
    ) -> Result<
        MarketDataServiceClient<InterceptedService<Channel, DefaultInterceptor>>,
        Box<dyn std::error::Error>,
    > {
        let client = MarketDataServiceClient::with_interceptor(
            channel,
            DefaultInterceptor {
                token: self.token.clone(),
            },
        );

        Ok(client)
    }

    pub async fn marketdata_stream(
        &self,
        channel: Channel,
    ) -> Result<
        MarketDataStreamServiceClient<InterceptedService<Channel, DefaultInterceptor>>,
        Box<dyn std::error::Error>,
    > {
        let client = MarketDataStreamServiceClient::with_interceptor(
            channel,
            DefaultInterceptor {
                token: self.token.clone(),
            },
        );

        Ok(client)
    }
}
