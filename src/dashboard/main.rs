use tinkinvestbot::{
    db::{self, DBConnector},
    model::{Instrument, OrderBook, Subscription},
};

use axum::{
    extract::{Path, Query, State},
    response::Html,
    routing::{get, post, put},
    Form, Json, Router,
};
use chrono::{Local, NaiveDateTime, TimeZone};
use hyper::StatusCode;
use serde::Deserialize;
use std::{io::Write, net::SocketAddr, process, sync::Arc};
use tokio::signal;
use tokio_util::sync::CancellationToken;
use tower_http::cors::CorsLayer;
use uuid::Uuid;

#[derive(Deserialize)]
struct Config {
    pg_address: String,
    bind_address: String,
}

#[tokio::main]
async fn main() {
    env_logger::Builder::new()
        .format(|buf, record| {
            writeln!(
                buf,
                "{} [{}] - {}",
                chrono::Local::now().format("%Y-%m-%dT%H:%M:%S"),
                record.level(),
                record.args()
            )
        })
        .filter(None, log::LevelFilter::Info)
        .init();

    let args: Vec<String> = std::env::args().collect();
    let conf_path = match args.get(1) {
        Some(path) => path,
        None => {
            log::error!("Pass config file");
            process::exit(1);
        }
    };

    let conf_reader = match std::fs::File::open(conf_path) {
        Ok(file) => file,
        Err(err) => {
            log::error!("Can't open config file: {}", err);
            process::exit(1);
        }
    };

    let config: Config = match serde_yaml::from_reader(conf_reader) {
        Ok(config) => config,
        Err(err) => {
            log::error!("Can't read correct config: {}", err);
            process::exit(1);
        }
    };

    let token = CancellationToken::new();

    let connector = match db::DBConnector::new(config.pg_address, token.clone()).await {
        Ok(connector) => connector,
        Err(err) => {
            log::error!("Can't connect with database: {}", err);
            process::exit(1);
        }
    };
    let shared_connector = Arc::new(connector);

    let shutdown_func = || async move {
        let ctrl_c = async {
            signal::ctrl_c()
                .await
                .expect("failed to install Ctrl+C handler");
        };

        tokio::select! {
            _ = ctrl_c => {
                token.cancel();
            },
            _ = token.cancelled() => {},
        }
    };

    let app = Router::new()
        .route("/healthcheck", get(healthcheck))
        .route("/subscriptions", post(add_subscription))
        .route("/subscriptions", put(change_subscription_status))
        .route("/subscriptions", get(get_all_subscriptions))
        .route("/orderbooks/:instrument_uid", get(get_order_book))
        .route(
            "/instruments/:instrument_uid/orderbooks",
            get(get_available_order_book_ids_by_instrument_uid),
        )
        .route("/instruments", get(get_available_instruments))
        .layer(CorsLayer::permissive())
        .with_state(shared_connector);

    let addr: SocketAddr = match config.bind_address.parse() {
        Ok(addr) => addr,
        Err(err) => {
            log::error!("Can't parse bind address: {}", err);
            process::exit(1);
        }
    };

    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .with_graceful_shutdown(shutdown_func())
        .await
        .unwrap();
}

async fn healthcheck() -> Html<&'static str> {
    Html("<h1>I'm fine</h1>")
}

async fn add_subscription(
    State(connector): State<Arc<DBConnector>>,
    Form(subscription): Form<Subscription>,
) -> StatusCode {
    match connector.add_subscription(subscription).await {
        Ok(()) => {
            return StatusCode::OK;
        }
        Err(err) => {
            log::error!("Can't add subscription: {}", err);
            return StatusCode::BAD_REQUEST;
        }
    };
}

async fn change_subscription_status(
    State(connector): State<Arc<DBConnector>>,
    Form(subscription): Form<Subscription>,
) -> StatusCode {
    match connector
        .change_subscription_status(subscription.instrument_uid, subscription.is_active)
        .await
    {
        Ok(()) => {
            return StatusCode::OK;
        }
        Err(err) => {
            log::error!("Can't change subscription status: {}", err);
            return StatusCode::BAD_REQUEST;
        }
    };
}

async fn get_all_subscriptions(
    State(connector): State<Arc<DBConnector>>,
) -> Result<Json<Vec<Subscription>>, StatusCode> {
    match connector.get_all_subscriptions().await {
        Ok(subs) => return Ok(Json(subs)),
        Err(err) => {
            log::error!("Can't get subscriptions: {}", err);
            return Err(StatusCode::BAD_REQUEST);
        }
    };
}

#[derive(Deserialize)]
struct Period {
    start: String,
    end: String,
}

async fn get_available_instruments(
    State(connector): State<Arc<DBConnector>>,
    Query(period): Query<Period>,
) -> Result<Json<Vec<Instrument>>, StatusCode> {
    let start = match NaiveDateTime::parse_from_str(period.start.as_str(), "%Y-%m-%d %H:%M:%S") {
        Ok(start) => start,
        Err(_) => return Err(StatusCode::BAD_REQUEST),
    };
    let end = match NaiveDateTime::parse_from_str(period.end.as_str(), "%Y-%m-%d %H:%M:%S") {
        Ok(end) => end,
        Err(_) => return Err(StatusCode::BAD_REQUEST),
    };

    let start = Local.from_local_datetime(&start).unwrap();
    let end = Local.from_local_datetime(&end).unwrap();

    match connector.get_available_instruments(start, end).await {
        Ok(instruments) => {
            return Ok(Json(instruments));
        }
        Err(err) => {
            log::error!("Can't get instruments: {}", err);
            return Err(StatusCode::BAD_REQUEST);
        }
    };
}

async fn get_available_order_book_ids_by_instrument_uid(
    State(connector): State<Arc<DBConnector>>,
    Path(instrument_uid): Path<String>,
    Query(period): Query<Period>,
) -> Result<Json<Vec<Uuid>>, StatusCode> {
    let start = match NaiveDateTime::parse_from_str(period.start.as_str(), "%Y-%m-%d %H:%M:%S") {
        Ok(start) => start,
        Err(_) => return Err(StatusCode::BAD_REQUEST),
    };
    let end = match NaiveDateTime::parse_from_str(period.end.as_str(), "%Y-%m-%d %H:%M:%S") {
        Ok(end) => end,
        Err(_) => return Err(StatusCode::BAD_REQUEST),
    };

    let start = Local.from_local_datetime(&start).unwrap();
    let end = Local.from_local_datetime(&end).unwrap();

    match connector
        .get_available_order_book_ids_by_instrument_uid(instrument_uid, start, end)
        .await
    {
        Ok(ids) => {
            return Ok(Json(ids));
        }
        Err(err) => {
            log::error!("Can't get instruments: {}", err);
            return Err(StatusCode::BAD_REQUEST);
        }
    };
}

async fn get_order_book(
    State(connector): State<Arc<DBConnector>>,
    Path(id): Path<Uuid>,
) -> Result<Json<OrderBook>, StatusCode> {
    match connector.get_order_book(id).await {
        Ok(ob) => {
            return Ok(Json(ob));
        }
        Err(err) => {
            log::error!("Can't get order book: {}", err);
            return Err(StatusCode::BAD_REQUEST);
        }
    };
}
