# This file contains examples of requests to interact with dashboard

## Add new subscription

```python
import requests

url = 'http://127.0.0.1:3000/subscriptions'
params = {'instrument_uid': 'de08affe-4fbd-454e-9fd1-46a81b23f870', 'name': 'positive'}

requests.post(url, data = params)
```

## Update subscription status

```python
import requests

url = 'http://127.0.0.1:3000/subscriptions'
params = {'instrument_uid': 'de08affe-4fbd-454e-9fd1-46a81b23f870', 'is_active': 'false'}

requests.put(url, data = params)
```

## Get all subscriptions

```python
import requests

url = 'http://127.0.0.1:3000/subscriptions'

requests.get(url).json()
```

## Get available order books by instrument_id for time inverval

```python
import requests

url = 'http://127.0.0.1:3000/instruments/de08affe-4fbd-454e-9fd1-46a81b23f870/orderbooks?start=2015-09-05 23:56:04&end=2025-09-05 23:56:04'

requests.get(url).json()
```

## Get order book by its id

```python
import requests

url = 'http://127.0.0.1:3000/orderbooks/de08affe-4fbd-454e-9fd1-46a81b23f870?start=2015-09-05 23:56:04&end=2025-09-05 23:56:04'

requests.get(url).json()
```

## Get available instruments for time interval

```python
import requests

url = 'http://127.0.0.1:3000/instruments?start=2015-09-05 23:56:04&end=2025-09-05 23:56:04'

requests.get(url).json()
```
