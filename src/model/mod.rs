use crate::tcs_grpc;

use chrono::{DateTime, Local};
use serde::{Deserialize, Serialize};
use std::{
    convert::{From, TryFrom},
    fmt,
};
use thiserror::Error;
use uuid::Uuid;

#[derive(Error, Debug)]
pub struct ConversionError {
    message: String,
}

impl fmt::Display for ConversionError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Quotation {
    pub units: i64,
    pub nano: i32,
}

impl From<tcs_grpc::Quotation> for Quotation {
    fn from(quotation: tcs_grpc::Quotation) -> Self {
        Quotation {
            units: quotation.units,
            nano: quotation.nano,
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Order {
    pub price: Quotation,
    pub quantity: i64,
}

impl TryFrom<tcs_grpc::Order> for Order {
    type Error = ConversionError;

    fn try_from(order: tcs_grpc::Order) -> Result<Order, Self::Error> {
        let quotation = match order.price {
            Some(quotation) => quotation,
            None => {
                return Err(ConversionError {
                    message: format!(
                        "Can't convert tcs_grpc::Order to model::Order : {:#?}",
                        order
                    ),
                })
            }
        };

        Ok(Order {
            quantity: order.quantity,
            price: quotation.into(),
        })
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OrderBook {
    pub id: Uuid,
    pub instrument_uid: String,
    pub bids: Vec<Order>,
    pub asks: Vec<Order>,
    pub timemstamptz: DateTime<Local>,
}

impl TryFrom<tcs_grpc::GetOrderBookResponse> for OrderBook {
    type Error = ConversionError;

    fn try_from(response: tcs_grpc::GetOrderBookResponse) -> Result<OrderBook, Self::Error> {
        let mut bids: Vec<Order> = Vec::new();
        for bid in response.bids.iter() {
            let order = TryInto::<Order>::try_into((*bid).clone())?;
            bids.push(order);
        }

        let mut asks: Vec<Order> = Vec::new();
        for ask in response.asks.iter() {
            let order = TryInto::<Order>::try_into((*ask).clone())?;
            asks.push(order);
        }

        return Ok(OrderBook {
            id: Uuid::new_v4(),
            instrument_uid: response.instrument_uid,
            bids: bids,
            asks: asks,
            timemstamptz: chrono::Local::now(),
        });
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Subscription {
    pub instrument_uid: String,
    #[serde(default)]
    pub name: String,
    #[serde(default = "subscription_is_active_default")]
    pub is_active: bool,
}

fn subscription_is_active_default() -> bool {
    true
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Instrument {
    pub instrument_uid: String,
    pub name: String,
}
