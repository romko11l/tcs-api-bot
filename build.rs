fn main() -> Result<(), Box<dyn std::error::Error>> {
    tonic_build::configure()
        .build_client(true)
        .build_server(false)
        .out_dir("./src/tcs_grpc/")
        .compile(
            &[
                "./proto/instruments.proto",
                "./proto/marketdata.proto",
                "./proto/operations.proto",
                "./proto/orders.proto",
                "./proto/sandbox.proto",
                "./proto/stoporders.proto",
                "./proto/users.proto",
            ],
            &["./proto/"],
        )?;

    std::fs::rename(
        "./src/tcs_grpc/tinkoff.public.invest.api.contract.v1.rs",
        "./src/tcs_grpc/mod.rs",
    )?;

    Ok(())
}
